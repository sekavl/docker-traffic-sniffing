FROM alpine:latest

RUN apk update && apk add tcpdump

RUN mkdir /tcpdump
WORKDIR /tcpdump

ENTRYPOINT [ "tcpdump" ]
