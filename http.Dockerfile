FROM python:3.6-alpine

RUN pip3 install aiohttp

EXPOSE 8080

COPY [".",  "./"]

CMD [ "http_server.py" ]
ENTRYPOINT [ "python3", "-u" ]
