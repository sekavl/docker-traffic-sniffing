import argparse
import asyncio
import random
import time

import aiohttp


async def fetch(client, host, port):
    if bool(random.getrandbits(1)):
        uri = 'http://{}:{}/{}'.format(host, port, 'health-check')
    else:
        uri = 'http://{}:{}/{}'.format(host, port, 'random')

    async with client.get(uri) as resp:
        return await resp.text()


async def main(host, port):
    async with aiohttp.ClientSession() as client:
        result = await fetch(client, host, port)
        logger(str(result))


def logger(text):
    if LOGGING:
        print(text)


def argParser():
    parser = argparse.ArgumentParser(description="HTTP scheduler (mocked)")
    parser.add_argument("--host", default="0.0.0.0", help="Host address")
    parser.add_argument("--port", default=8080, type=int, help="Port number")
    parser.add_argument("--log", default=1, type=int, help="Printing log statements")
    return parser.parse_args()


if __name__ == "__main__":
    arguments = argParser()
    host = str(arguments.host)
    port = arguments.port
    LOGGING = arguments.log
    loop = asyncio.get_event_loop()
    while True:
        try:
            loop.run_until_complete(main(host, port))
        except:
            logger("Host is not reachable!")
        time.sleep(1)
