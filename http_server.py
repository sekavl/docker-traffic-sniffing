import random
import aiohttp
from aiohttp import web
import argparse


class HttpServer():

    def __init__(self, host, port):
        app = web.Application()
        app.add_routes([
            web.get('/health-check', self.healthCheck),
            web.get('/random', self.randomResponse)
        ])
        web.run_app(app, host=host, port=port)


    async def healthCheck(self, request):
        logger('Health-check hit')
        return web.json_response({'status': 'OK'}, status=200)


    async def randomResponse(self, request):
        logger('Random request hit')
        return web.json_response({'random number': str(random.random())}, status=202)


def logger(text):
    if LOGGING:
        print(text)


def argParser():
    parser = argparse.ArgumentParser(description="HTTP scheduler (mocked)")
    parser.add_argument("--host", default="0.0.0.0", help="Host address")
    parser.add_argument("--port", default=8080, type=int, help="Port number")
    parser.add_argument("--log", default=1, type=int, help="Printing log statements")
    return parser.parse_args()


if __name__ == "__main__":
    arguments = argParser()
    host = str(arguments.host)
    port = arguments.port
    LOGGING = arguments.log
    logger("Starting HTTP Server...")
    HttpServer(host, port)
